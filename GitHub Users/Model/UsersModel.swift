//
//  UsersModel.swift
//  GitHub Users
//
//  Created by Igor Lungis on 1/18/20.
//  Copyright © 2020 Igor Lungis. All rights reserved.
//

import Foundation

struct UsersModel: Decodable {
    
    var login: String?
    var id: Int?
    var avatar_url: String?
    var url: String?
    
}
