//
//  DetailsModel.swift
//  GitHub Users
//
//  Created by Igor Lungis on 1/18/20.
//  Copyright © 2020 Igor Lungis. All rights reserved.
//

import Foundation

struct DetailsModel: Decodable {
    
    var avatar_url: String?
    var name: String?
    var email: String?
    var company: String?
    var followers: Int?
    var following: Int?
    var created_at: String?
    
}
