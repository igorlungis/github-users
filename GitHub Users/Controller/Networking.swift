//
//  Networking.swift
//  GitHub Users
//
//  Created by Igor Lungis on 1/21/20.
//  Copyright © 2020 Igor Lungis. All rights reserved.
//

import Foundation
import Alamofire

class Networking {
    
    static let shared = Networking()
    
    public func fetchDataUsersModel (urlString: String, comletion: @escaping (Any) -> ()) {
        
        guard let url = URL(string: urlString) else { return }
        AF.request(url).responseJSON { (response) in
            do {
                let json = try JSONDecoder().decode([UsersModel].self, from: response.data!)
                comletion(json)
            } catch let error {
                    print("JSON ERROR -->>>", error)
              }
        }
    }
    
    public func fetchDataDetailsModel (urlString: String, comletion: @escaping (Any) -> ()) {
        
        guard let url = URL(string: urlString) else { return }
        AF.request(url).responseJSON { (response) in
            do {
                let json = try JSONDecoder().decode(DetailsModel.self, from: response.data!)
                comletion(json)
            } catch let error {
                    print("JSON ERROR -->>>", error)
              }
        }
    }
    
}

