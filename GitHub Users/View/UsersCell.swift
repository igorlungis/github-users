//
//  UsersCell.swift
//  GitHub Users
//
//  Created by Igor Lungis on 1/18/20.
//  Copyright © 2020 Igor Lungis. All rights reserved.
//

import UIKit

class UsersCell: UITableViewCell {
    
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var login: UILabel!
    @IBOutlet weak var id: UILabel!
    @IBOutlet weak var pageNumber: UILabel!
    
    var urlString: String?
    

}
