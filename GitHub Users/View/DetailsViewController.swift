//
//  DetailsViewController.swift
//  GitHub Users
//
//  Created by Igor Lungis on 1/18/20.
//  Copyright © 2020 Igor Lungis. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire

class DetailsViewController: UIViewController {

    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var company: UILabel!
    @IBOutlet weak var following: UILabel!
    @IBOutlet weak var followers: UILabel!
    @IBOutlet weak var createdAt: UILabel!
    
    var userUrl: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchDetailsData()
    }
    
    @IBAction func backButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}

extension DetailsViewController {
   
    func fetchDetailsData() {
        
        Networking.shared.fetchDataDetailsModel(urlString: self.userUrl!) { (json) in
            let userDetails = (json as! DetailsModel)
            DispatchQueue.main.async {
                self.name.text = "Name: " + (userDetails.name ?? "-")
                self.email.text = "Email: " + (userDetails.email ?? "-")
                self.company.text = "Company: " + (userDetails.company ?? "-")
                if let following = userDetails.following {
                    self.following.text = "Following: " + String(following)
                } else { self.following.text = "Following: -" }
                if let followers = userDetails.followers {
                    self.followers.text = "Followers: " + String(followers)
                } else { self.followers.text = "Followers: -" }
                self.createdAt.text = "Created at: " + (userDetails.created_at ?? "-")
            }
            if let url = URL(string: userDetails.avatar_url!) {
                self.avatar!.sd_setImage(with: url, completed: nil)
            }
        }
    }
    
}
