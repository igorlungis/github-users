//
//  UsersTableViewController.swift
//  GitHub Users
//
//  Created by Igor Lungis on 1/17/20.
//  Copyright © 2020 Igor Lungis. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire

class UsersViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    let myRefreshControl: UIRefreshControl = {
      let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: .valueChanged)
        return refreshControl
    }()
    
    private var usersData: [UsersModel]?
    var detailsURL: String?
    var pageArray = [1]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchUsersData()
        tableView.refreshControl = myRefreshControl
    }
    
    @objc private func refresh(sender: UIRefreshControl) {
        fetchUsersData()
        sender.endRefreshing()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return usersData?.count ?? 0
    }


    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "usersCell", for: indexPath) as! UsersCell

        cell.login.text = self.usersData?[indexPath.item].login ?? ""
        cell.id.text = "id: " + String(describing: (self.usersData?[indexPath.item].id)!)
        
        if let urlImage = URL(string: (self.usersData?[indexPath.item].avatar_url)!) {
            cell.avatar.sd_setImage(with: urlImage, completed: nil) }
        cell.avatar.layer.cornerRadius = 15
        cell.avatar.clipsToBounds = true
       
        cell.pageNumber.text = "\(self.pageArray[indexPath.row])"
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == pageArray.count - 1 {
            morePage()
        }
    }
        
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.detailsURL = self.usersData?[indexPath.item].url
        let detailsViewController = storyboard!.instantiateViewController(withIdentifier: "detailsVC") as! DetailsViewController
        detailsViewController.userUrl = self.usersData?[indexPath.item].url
        detailsViewController.modalTransitionStyle = .coverVertical
        detailsViewController.modalPresentationStyle = .fullScreen
        self.present(detailsViewController, animated: true)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension UsersViewController {
    
    private func fetchUsersData() {
        
        Networking.shared.fetchDataUsersModel(urlString: "https://api.github.com/users") { (json) in
            self.usersData = (json as! [UsersModel])
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    private func morePage() {
        for _ in 0...usersData!.count {
            pageArray.append(pageArray.last! + 1)
        }
    }
    
}

